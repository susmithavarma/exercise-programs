package ExceptionHandling;

public class Try {
	public static void main(String args[]) {
		try {
			String s="hello";
			int i=Integer.parseInt(s);
			System.out.println(s);
		}
		catch(Exception e) {
			System.out.println(e);
			System.out.println("hello");
		}
		finally {
			System.out.println("done");
		}
	}

}
